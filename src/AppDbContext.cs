using MDM.SessionClock.Models;
using Microsoft.EntityFrameworkCore;

namespace MDM.SessionClock
{
    public class AppDbContext : DbContext
    {
        public DbSet<Session> Sessions { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=session-clock.db");
        }
    }
}