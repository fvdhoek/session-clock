
using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace MDM.SessionClock.Controllers
{
    [Route("api/[controller]")]
    public class AppVersionController : Controller
    {
        public AppVersionController()
        {
        }

        [HttpGet("[action]")]
        public string Get()
        {
            return Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
        }
    }
}
