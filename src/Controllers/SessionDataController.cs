using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MDM.SessionClock.Models;
using MDM.SessionClock.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace MDM.SessionClock.Controllers
{
    [Route("api/[controller]")]
    public class SessionDataController : Controller
    {
        private readonly IMapper _mapper;
        public SessionDataController(IMapper mapper)
        {
            _mapper = mapper;    
        }

        [HttpGet("[action]/{onlyActive?}")]
        public IEnumerable<SessionEditItem> GetSessions(bool onlyActive=false)
        {
            using (var db = new AppDbContext())
            {
                var query = db.Sessions.OrderBy(x => x.Id);
                if (onlyActive) {
                    query = db.Sessions.Where(x => x.Active == true).OrderBy(x => x.Id);
                }
                var viewModel = query.ToList().Select(x => _mapper.Map<SessionEditItem>(x));
                return viewModel;
            }
        }

        [HttpPut("[action]")]
        public IEnumerable<SessionEditItem> UpdateSession([FromBody]SessionEditItem data)
        {
            if (data == null) {
                throw new ArgumentNullException(nameof(data));
            }

            using (var db = new AppDbContext())
            {
                var entity = db.Sessions.FirstOrDefault(x => x.Id == data.Id);
                db.Sessions.Update(entity);
                _mapper.Map<SessionEditItem, Session>(data, entity);
                db.SaveChanges();
            }

            return GetSessions();
        }
    }
}
