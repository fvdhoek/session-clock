using AutoMapper;
using MDM.SessionClock.Models;
using MDM.SessionClock.ViewModels;

namespace session_clock.Automapper.Profiles
{
    public class SessionProfile: Profile
    {
        public SessionProfile()
        {
            CreateMap<Session, SessionEditItem>();
        }
    }
}