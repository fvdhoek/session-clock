class SessionStore extends Object {
    getActive() {
        return fetch('api/SessionData/GetSessions?onlyActive=true');
    }
}
export { SessionStore };