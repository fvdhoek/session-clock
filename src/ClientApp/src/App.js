import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { SessionData } from './components/SessionData';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faEdit, faSave, faCheck } from '@fortawesome/free-solid-svg-icons';
import { SessionStore } from './SessionStore';

library.add([faEdit, faSave, faCheck]);

const store = new SessionStore();

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={() => <Home sessionStore={store} />}  />
        <Route path='/session-data' component={SessionData} />
      </Layout>
    );
  }
}
