import React, { Component } from 'react';
import { Spinner } from 'reactstrap';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import MP3_FILE from '../media/beep-07.mp3';


const moment = extendMoment(Moment);
export const NEXTSESSION_TRESHOLD_IN_MINUTES = 60 * 3
export const NEXTSESSION_RED_TRESHOLD_IN_MINUTES = 10

class Home extends Component {
  static displayName = Home.name;

  constructor (props) {
    super(props);
    this.state = { sessions: [], date: moment(), loading: true };
    this.audioFile = new Audio(MP3_FILE);
    this.refreshData();
  }

  get store() {
    return this.props.sessionStore;
  }

  componentDidMount() {
    this.intervalID = setInterval(
      () => this.tick(),
      1000
    );
  }
  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  tick() {
    this.setState({
      date: moment()
    });
  }

  refreshData() {
    this.store.getActive()
      .then(response => response.json())
      .then(data => {
        this.setState({ sessions: data.map(row => { 
          row.utcStart = moment.utc(row.utcStart).local().format();
          row.utcEnd = moment.utc(row.utcEnd).local().format();
          return row;
        }).filter((row) => row.active === true).sort((rowA, rowB) => {
          if (moment(rowA.utcStart) > moment(rowB.utcStart)) {
            return 1;
          } else if (moment(rowA.utcStart) === moment(rowB.utcStart)) {
            return 0;
          } else {
            return -1;
          }
        }), loading: false });
      }).catch((err) => console.log(err));
  }
  
  getNextSession() {
    const result = this.state.sessions.filter((value) => {
      const startDate = moment(value.utcStart)
      if (startDate.diff(this.getNow()) > 0) {
        return true;
      }
      return false;
    }); 
    return result.length > 0 ? result[0] : null;
  }

  getCurrentSession() {
    const result = this.state.sessions.filter((value) => {
      const startDate = moment(value.utcStart);
      const endDate = moment(value.utcEnd);
      if (startDate.diff(this.getNow()) < 0 && endDate.diff(this.getNow()) > 0) {
        return true;
      }
      return false;
    });
    return result.length > 0 ? result[0] : null;
  }

  sessionActive() {
    const currentSession = this.getCurrentSession();
    if (!currentSession) {
      return false;
    }
    return true;
  }

  upcomingSessionInMinutes() {
    const nextSession = this.getNextSession();
    if (!nextSession) {
      return -1;
    }
    var startDate = moment(nextSession.utcStart)
      , date  = this.getNow()
      , diff = startDate.diff(date, 'minutes')
    return diff
  }

  sessionUpcoming(withinMinutes) {
    if (this.upcomingSessionInMinutes() === -1) {
      return false;
    }
    return this.upcomingSessionInMinutes() <= withinMinutes; 
  }

  getNow() {
    return this.state.date;
  }

  formatDuration(duration) {
    return `${duration.hours().toString().padStart(2, '0')}:${duration.minutes().toString().padStart(2, '0')}:${duration.seconds().toString().padStart(2, '0')}`;
  }

  getDurationToNextSession() {
    const nextSession = this.getNextSession();
    if (!nextSession) {
      return null;
    }
    const startDate = moment(nextSession.utcStart)
            , date  = this.getNow();
    const duration = moment.duration(startDate.diff(date));
    return this.formatDuration(duration);
  }

  getDurationToEndOfSession() {
    const nextSession = this.getCurrentSession();
    if (!nextSession) {
      return null;
    }
    const endDate = moment(nextSession.utcEnd)
            , date  = this.getNow();
    const duration = moment.duration(endDate.diff(date));
    return this.formatDuration(duration);
  }

  render () {
    //in the first minute of the red session play a sound to alert the people on the floor.
    const upcomingSessionInMinutes = this.upcomingSessionInMinutes();
    if (upcomingSessionInMinutes !== -1 
      && upcomingSessionInMinutes <= NEXTSESSION_RED_TRESHOLD_IN_MINUTES 
      && upcomingSessionInMinutes >= (NEXTSESSION_RED_TRESHOLD_IN_MINUTES - 1))
    {
      if ((this.getNow().second() % 2) === 0) {
        this.audioFile.play();
      }
    }
   
    let session = <div className="session none">NEXT SESSION: N/A</div>
    let clock = <div className="clock text-center">{this.getNow().format("HH:mm:ss")}</div>;
    if (this.sessionActive()) {
      session = <div className="session">CURRENT SESSION: {this.getCurrentSession().name}</div>
      clock = <div className="clock green text-center">{this.getDurationToEndOfSession()}</div>;
    } else if (this.sessionUpcoming(NEXTSESSION_RED_TRESHOLD_IN_MINUTES)) {
      session = <div className="session">NEXT SESSION: {this.getNextSession().name}</div>
      clock = <div className="clock red text-center">{this.getDurationToNextSession()}</div>;
    } else if (this.sessionUpcoming(NEXTSESSION_TRESHOLD_IN_MINUTES) ) {
      session = <div className="session">NEXT SESSION: {this.getNextSession().name}</div>
      clock = <div className="clock text-center">{this.getDurationToNextSession()}</div>; 
    }

    return this.state.loading
      ? (
        <div className="h-100 invert-colors">
          <div className="row h-100">
            <div className="col-lg-12 parent">
              <Spinner color="primary" style={{ width: '100px', height: '100px', marginLeft: '48%' }} />
            </div>
          </div>
        </div>)
      : (
        <div className="h-100 invert-colors">
          <div className="row h-100">
            <div className="col-lg-1 text-center parent">
            </div>
            <div className="col-lg-10 parent">
              {session}
              {clock}
              <div className="clock-date" style={{float: 'right'}}>{this.getNow().format("MMM Do HH:mm")}</div>
            </div>
            <div className="col-lg-1 text-center parent">
            </div>
          </div>
        </div>
    );
  }
}


export { Home };