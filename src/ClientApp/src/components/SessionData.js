import moment from 'moment';
import React, { Component } from 'react';
import { Container, Button, Spinner, Input } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Moment from 'react-moment';
import DatePicker, { registerLocale } from 'react-datepicker';
import SessionRow from './Models';
import "react-datepicker/dist/react-datepicker.css";
import enGB from 'date-fns/locale/en-GB';
registerLocale('en-GB', enGB);



export class SessionData extends Component {
  static displayName = SessionData.name;

  constructor (props) {
    super(props);
    this.state = { sessions: [], loading: true, appVersion: "n/a" };
    this.refreshList();
  }

  refreshList() {
    fetch('api/SessionData/GetSessions')
      .then(response => response.json())
      .then(data => {
        this.setState({ sessions: data.map(row => { 
          row.utcStart = moment.utc(row.utcStart).local().format();
          row.utcEnd = moment.utc(row.utcEnd).local().format();
          return new SessionRow(row, false);
        }), loading: false });
      });
    fetch('api/AppVersion/Get')
      .then(response => response.text())
      .then(version => this.setState({ appVersion: version }));
  }

  handleChange(index, dataType, value) {
    const newState = this.state.sessions.map((item, i) => {
      if (i === index) {
        return new SessionRow({...item.value, [dataType]: value}, item.isEditing);
      }
      return item;
    });

    this.setState({
       sessions: newState
    });
  }

  handleEdit(index) {
    const newState = this.state.sessions.map((item, i) => {
      if (i === index) {
        return new SessionRow({...item.value}, true);
      }
      return item;
    });

    this.setState({
      sessions: newState
   });
  }

  handleSave(index) {
    var session = {...this.state.sessions[index].value};
    session.utcStart = moment.utc(session.utcStart);
    session.utcEnd = moment.utc(session.utcEnd);
    
    fetch('api/SessionData/UpdateSession', {
        method: 'put',
        body: JSON.stringify(session),
        headers: {
          "Content-Type": "application/json"
        },
      })
      .then(response => response.json())
      .then(data => this.refreshList());
  }

  formatDuration(duration) {
    return `${duration.hours().toString().padStart(2, '0')}:${duration.minutes().toString().padStart(2, '0')}:${duration.seconds().toString().padStart(2, '0')}`;
  }

  renderSessionTable () {
    return (
      <table className='table table-striped'>
        <thead>
          <tr>
            <th style={{ width: '20%'}}>Name</th>
            <th style={{ width: '20%'}}>Start</th>
            <th style={{ width: '20%'}}>End</th>
            <th style={{ width: '10%'}}>Active</th>
            <th style={{ width: '20%'}}>Duration</th>
            <th style={{ width: '10%'}}>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {this.state.sessions.map((session, index) => {
            
            return (
            <tr key={session.value.id}>
              <td>{ session.isEditing 
                ? <Input placeholder="session name" value={session.value.name} 
                    onChange={e => this.handleChange(index, 'name',  e.target.value) } /> 
                : <span>{session.value.name}</span> }</td>
              <td>{ session.isEditing 
                ? <DatePicker 
                    className="form-control"
                    selected={Date.parse(session.value.utcStart)}
                    onChange={dateTime => this.handleChange(index, 'utcStart',  dateTime) }
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={5}
                    dateFormat="MM/dd/yy HH:mm"
                    timeCaption="time"
                    locale="en-GB"
                  />
                : <Moment format="ddd Do, HH:mm" withTitle>{session.value.utcStart}</Moment> }</td>
              <td>{ session.isEditing 
                ? <DatePicker 
                    className="form-control"
                    selected={Date.parse(session.value.utcEnd)}
                    onChange={dateTime => this.handleChange(index, 'utcEnd', dateTime) }
                    showTimeSelect
                    timeFormat="HH:mm"
                    timeIntervals={5}
                    dateFormat="MM/dd/yy HH:mm"
                    timeCaption="time"
                    locale="en-GB"
                  />
                : <Moment format="ddd Do, HH:mm" withTitle>{session.value.utcEnd}</Moment> }</td>
              <td>{ session.isEditing
                ? <Input addon type="checkbox" 
                  aria-label="Check to activate this session" 
                  defaultChecked={session.value.active} 
                  onChange={e => this.handleChange(index, 'active', e.target.checked) } />
                : session.value.active ? <FontAwesomeIcon icon="check" /> : <span>&nbsp;</span> }</td>
              <td>{ this.formatDuration(moment.duration(moment(session.value.utcEnd).diff(moment(session.value.utcStart)))) }</td>
              <td>{ session.isEditing 
                ? <Button color="primary" onClick={(e) => this.handleSave(index)}><FontAwesomeIcon icon="save" /></Button>
                : <Button color="secondary" onClick={(e) => this.handleEdit(index)}><FontAwesomeIcon icon="edit" /></Button> }</td>
            </tr>)
          })}
        </tbody>
      </table>
    );
  }

  render () {
    let contents = this.state.loading
      ? <Spinner color="primary" />
      : this.renderSessionTable();

    return (
      <Container>
        <h1>Sessions</h1>
        <p>Edit the sessions for this weekend below.</p>
        <div className="scrollable">
          {contents}
        </div>
        <p className="credits">Created by Frank van den Hoek, Trancon B.V. <br /><span>Version: {this.state.appVersion}</span></p>
      </Container>
    );
  }
}
