import React from 'react';
import moment from 'moment';
import { create } from "react-test-renderer";
import { Home } from './Home';

const SESSIONS_EMPTY = [];


class MockSessionStore extends Object {
  response = null;

  constructor(sessions) {
    super();
    this.response =  { json: function() { return sessions; } };
  }
  getActive() {
    return new Promise(
      (resolve, reject) => { 
        resolve(this.response);
    });
  }
}

const getComponentInstance = async function(sessions) {
  const component = create(<Home sessionStore={new MockSessionStore(sessions)} />);
  const instance = component.getInstance();
  instance.audioFile = { play: function() {} };
  await instance.componentDidMount();
  return instance;
}

describe("Home component", () => {
  
  it("loads without errors", async () => {
    const component = create(<Home sessionStore={new MockSessionStore(SESSIONS_EMPTY)} />);
    const instance = component.getInstance();
    await instance.componentDidMount();
  });

  it("when sessions are empty no session is active", async () => {
    const instance = await getComponentInstance(SESSIONS_EMPTY);
    expect(instance.sessionActive()).toEqual(false);
  });

  it("when sessions are empty no session is upcoming", async () => {
    const instance = await getComponentInstance(SESSIONS_EMPTY);
    expect(instance.sessionUpcoming()).toEqual(false);
  });

  it("an upcoming session will show up as upcoming", async () => {
    const sessions = [
      { id: 2, name: 'FP1', utcStart: moment().utc().add(20, 'minutes'), utcEnd: moment().utc().add(2, 'hours'), active: true}
    ];
    const instance = await getComponentInstance(sessions);
    const withinMinutes = 30;
    expect(instance.sessionUpcoming(withinMinutes)).toEqual(true);
  });

  it("a session in the past will not show up as upcoming", async () => {
    const sessions = [
      { id: 1, name: 'FP1', utcStart: '2019-01-01T10:00:00Z', utcEnd: '2019-01-01T12:00:00Z', active: true},
      { id: 2, name: 'FP2', utcStart: moment().utc().add(20, 'minutes'), utcEnd: moment().utc().add(2, 'hours'), active: true}
    ];
    const instance = await getComponentInstance(sessions);
    expect(instance.sessionUpcoming(30)).toEqual(true);
    const session = instance.getNextSession();
    expect(session.name).toEqual(sessions[1].name);
  });

  it("unordered sessions will be ordered correctly", async () => {
    const sessions = [
      { id: 2, name: 'FP2', utcStart: moment().utc().add(15, 'minutes'), utcEnd: moment().utc().add(20, 'minutes'), active: true},
      { id: 3, name: 'FP3', utcStart: moment().utc().add(25, 'minutes'), utcEnd: moment().utc().add(30, 'minutes'), active: true},
      { id: 1, name: 'FP1', utcStart: moment().utc().add(5, 'minutes'), utcEnd: moment().utc().add(10, 'minutes'), active: true}
    ];
    const instance = await getComponentInstance(sessions);
    const session = instance.getNextSession();
    expect(session.name).toEqual(sessions[2].name);
  });

  it("unactive sessions will be ignored", async () => {
    const sessions = [
      { id: 2, name: 'FP2', utcStart: moment().utc().add(15, 'minutes'), utcEnd: moment().utc().add(20, 'minutes'), active: true},
      { id: 3, name: 'FP3', utcStart: moment().utc().add(25, 'minutes'), utcEnd: moment().utc().add(30, 'minutes'), active: true},
      { id: 1, name: 'FP1', utcStart: moment().utc().add(5, 'minutes'), utcEnd: moment().utc().add(10, 'minutes'), active: false}
    ];
    const instance = await getComponentInstance(sessions);
    const session = instance.getNextSession();
    expect(session.name).toEqual(sessions[0].name);
  });
  
  
});