class SessionRow extends Object {
    constructor(value, isEditing) {
      super(value);
      this.value = value;
      this.isEditing = isEditing;
    }
}

export default SessionRow;