using System;
using Microsoft.EntityFrameworkCore;

namespace MDM.SessionClock.Models
{
    public class Session
    {
        
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime UtcStart { get; set; }
            public DateTime UtcEnd { get; set; }
            public bool Active { get; set; }
    }
}