using System;

namespace MDM.SessionClock.ViewModels
{
    public class SessionEditItem
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime UtcStart { get; set; }
            public DateTime UtcEnd { get; set; }
            public bool Active { get; set; }
        }
}